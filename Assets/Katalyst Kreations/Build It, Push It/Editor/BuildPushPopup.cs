﻿using UnityEditor;
using UnityEngine;

namespace KatalystKreations.BuildPush {

    public class BuildPushPopup : PopupWindowContent {
        private BuildPushData data;
        private BuildPushEditor window;

        public override void OnOpen() {
            window = (BuildPushEditor)EditorWindow.GetWindow(typeof(BuildPushEditor));
            if (window != null)
                data = window.data;
        }

        public override Vector2 GetWindowSize() {
            return new Vector2(200, 150);
        }

        public override void OnGUI(Rect rect) {
            GUILayout.Label(new GUIContent("What kind of update is this?", "These refer back to the version numbers of the auto-incrementation system."), EditorStyles.boldLabel);
            window.obj.FindProperty("hotfixToggle").boolValue = EditorGUILayout.Toggle("Hotfix", window.obj.FindProperty("hotfixToggle").boolValue);
            window.obj.FindProperty("patchToggle").boolValue = EditorGUILayout.Toggle("Patch", window.obj.FindProperty("patchToggle").boolValue);
            window.obj.FindProperty("contentToggle").boolValue = EditorGUILayout.Toggle("Content Addition", window.obj.FindProperty("contentToggle").boolValue);
            window.obj.FindProperty("expansionToggle").boolValue = EditorGUILayout.Toggle("Expansion", window.obj.FindProperty("expansionToggle").boolValue);
            GUILayout.Space(25);
            if (GUILayout.Button(new GUIContent("Okay", "Commence the selected operation."))) {
                window.StartBuildPush();
            }
            window.obj.ApplyModifiedProperties();
            window.obj.Update();
        }
    }
}