﻿using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

#endif

[CreateAssetMenu(menuName = "Variables/String")]
public class StringVariable : ScriptableObject
{
#if UNITY_EDITOR

    [Multiline]
    public string DeveloperDescription = "";
#endif

    [SerializeField]
    private string value;
    public string Value
    {
        get { return value; }
        private set { this.value = value; }
    }

    public delegate void ChangeEvent();
    public ChangeEvent OnValueChanged;

    [SerializeField]
    private StringReference defaultValue;

    private void OnEnable()
    {
        if (defaultValue != null)
            Value = defaultValue.Value;
#if UNITY_EDITOR
        EditorApplication.playModeStateChanged += (x) =>
        {
            if (EditorApplication.isPlayingOrWillChangePlaymode == true &&
                EditorApplication.isPlaying == false)
            {
                Value = defaultValue.Value;
            }
        };
#endif
    }

    public void SetValue(string value)
    {
        Value = value;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }

    public void SetValue(StringVariable value)
    {
        Value = value.Value;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }

    public void ApplyChange(string amount)
    {
        Value += amount;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }

    public void ApplyChange(StringVariable amount)
    {
        Value += amount.Value;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }
}