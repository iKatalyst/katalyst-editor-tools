﻿using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

#endif

[CreateAssetMenu(menuName = "Variables/Int")]
public class IntVariable : ScriptableObject
{
#if UNITY_EDITOR

    [Multiline]
    public string DeveloperDescription = "";
#endif

    public delegate void ChangeEvent();
    public ChangeEvent OnValueChanged;

    [SerializeField]
    private int value;
    public int Value
    {
        get { return value; }
        private set { this.value = value; }
    }

    [SerializeField]
    private IntReference defaultValue;

    private void OnEnable()
    {
        if (defaultValue != null)
            Value = defaultValue;
#if UNITY_EDITOR
        EditorApplication.playModeStateChanged += (x) =>
        {
            if (EditorApplication.isPlayingOrWillChangePlaymode == true &&
                EditorApplication.isPlaying == false)
            {
                Value = defaultValue.Value;
            }
        };
#endif
    }

    public void SetValue(int value)
    {
        Value = value;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }

    public void SetValue(IntVariable value)
    {
        Value = value.Value;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }

    public void ApplyChange(int amount)
    {
        Value += amount;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }

    public void ApplyChange(IntVariable amount)
    {
        Value += amount.Value;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }
}