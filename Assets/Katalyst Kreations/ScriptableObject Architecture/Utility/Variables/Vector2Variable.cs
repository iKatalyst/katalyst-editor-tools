﻿using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

#endif

[CreateAssetMenu(menuName = "Variables/Vector2")]
public class Vector2Variable : ScriptableObject
{
#if UNITY_EDITOR

    [Multiline]
    public string DeveloperDescription = "";
#endif

    [SerializeField]
    private Vector2 value;
    public Vector2 Value
    {
        get { return value; }
        private set { this.value = value; }
    }

    public delegate void ChangeEvent();
    public ChangeEvent OnValueChanged;

    [SerializeField]
    private Vector2Reference defaultValue;

    private void OnEnable()
    {
        if (defaultValue != null)
            Value = defaultValue.Value;
#if UNITY_EDITOR
        EditorApplication.playModeStateChanged += (x) =>
        {
            if (EditorApplication.isPlayingOrWillChangePlaymode == true &&
                EditorApplication.isPlaying == false)
            {
                Value = defaultValue.Value;
            }
        };
#endif
    }

    public void SetValue(Vector2 value)
    {
        Value = value;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }

    public void SetValue(Vector2Variable value)
    {
        Value = value.Value;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }

    public void ApplyChange(Vector2 amount)
    {
        Value += amount;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }

    public void ApplyChange(Vector2Variable amount)
    {
        Value += amount.Value;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }
}