﻿using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

#endif

[CreateAssetMenu(menuName = "Variables/Vector3")]
public class Vector3Variable : ScriptableObject
{
#if UNITY_EDITOR

    [Multiline]
    public string DeveloperDescription = "";
#endif

    [SerializeField]
    private Vector3 value;
    public Vector3 Value
    {
        get { return value; }
        private set { this.value = value; }
    }

    public delegate void ChangeEvent();
    public ChangeEvent OnValueChanged;

    [SerializeField]
    private Vector3Reference defaultValue;

    private void OnEnable()
    {
        if (defaultValue != null)
            Value = defaultValue.Value;
#if UNITY_EDITOR
        EditorApplication.playModeStateChanged += (x) =>
        {
            if (EditorApplication.isPlayingOrWillChangePlaymode == true &&
                EditorApplication.isPlaying == false)
            {
                Value = defaultValue.Value;
            }
        };
#endif
    }

    public void SetValue(Vector3 value)
    {
        Value = value;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }

    public void SetValue(Vector3Variable value)
    {
        Value = value.Value;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }

    public void ApplyChange(Vector3 amount)
    {
        Value += amount;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }

    public void ApplyChange(Vector3Variable amount)
    {
        Value += amount.Value;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }
}