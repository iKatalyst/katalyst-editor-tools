﻿using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

#endif

[CreateAssetMenu(menuName = "Variables/Int64")]
public class Int64Variable : ScriptableObject
{
#if UNITY_EDITOR

    [Multiline]
    public string DeveloperDescription = "";
#endif

    [SerializeField]
    private long value;
    public long Value
    {
        get { return value; }
        private set { this.value = value; }
    }

    [SerializeField]
    private Int64Reference defaultValue;

    private void OnEnable()
    {
        if (defaultValue != null)
            Value = defaultValue.Value;
#if UNITY_EDITOR
        EditorApplication.playModeStateChanged += (x) =>
        {
            if (EditorApplication.isPlayingOrWillChangePlaymode == true &&
                EditorApplication.isPlaying == false)
            {
                Value = defaultValue.Value;
            }
        };
#endif
    }

    public delegate void ChangeEvent();
    public ChangeEvent OnValueChanged;

    public void SetValue(long value)
    {
        Value = value;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }

    public void SetValue(Int64Variable value)
    {
        Value = value.Value;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }

    public void ApplyChange(long amount)
    {
        Value += amount;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }

    public void ApplyChange(Int64Variable amount)
    {
        Value += amount.Value;
        if (OnValueChanged != null)
            OnValueChanged.Invoke();
    }
}