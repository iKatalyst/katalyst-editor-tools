﻿namespace KatalystKreations
{
    using System.Collections;
    using UnityEngine;
    using UnityEngine.EventSystems;

    [RequireComponent(typeof(AudioSource))]
    public class AudioTriggerEvent : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
    {
        public AudioTriggerMode triggerMode;
        public AudioTriggerTypesUI triggerUIType;
        public AudioTriggerTypes2D trigger2DType;
        public AudioTriggerTypes3D trigger3DType;
        public DestroyType destroyAfterPlay;

        public bool randomizedPitch;
        public float randomizedPitchRangeMin;
        public float randomizedPitchRangeMax;

        public bool randomizedVolume;
        public float randomizedVolumeRangeMin;
        public float randomizedVolumeRangeMax;

        public bool loop;
        public float interval;
        public int count;
        public AudioTriggerTypesUI stopTriggerUIType;
        public AudioTriggerTypes2D stopTrigger2DType;
        public AudioTriggerTypes3D stopTrigger3DType;

        private AudioSource _audioSource;
        private bool isLooping;

        private void Start()
        {
            _audioSource = GetComponent<AudioSource>();

            if (_audioSource == null)
            {
                Debug.LogError("AudioTrigger::Start::Error -- No AudioSource found on this GameObject.");
                return;
            }

            if (triggerMode == AudioTriggerMode.OnStart)
            {
                _audioSource.Play();
                RandomizeSounds();
                Loop();
                DestroyAfter();
            }
        }

        private void OnMouseEnter()
        {
            if (isLooping == false)
            {
                if (triggerMode == AudioTriggerMode.TwoD && trigger2DType == AudioTriggerTypes2D.OnMouseEnter)
                {
                    _audioSource.Play();
                    RandomizeSounds();
                    Loop();
                    DestroyAfter();
                }

                if (triggerMode == AudioTriggerMode.ThreeD && trigger3DType == AudioTriggerTypes3D.OnMouseEnter)
                {
                    _audioSource.Play();
                    RandomizeSounds();
                    Loop();
                    DestroyAfter();
                }
            }

            if (isLooping == true)
            {
                if (StopLoop(AudioTriggerMode.TwoD, AudioTriggerTypes2D.OnMouseEnter))
                    return;
                if (StopLoop(AudioTriggerMode.ThreeD, AudioTriggerTypes3D.OnMouseEnter))
                    return;
            }
        }

        private void OnMouseExit()
        {
            if (isLooping == false)
            {
                if (triggerMode == AudioTriggerMode.TwoD && trigger2DType == AudioTriggerTypes2D.OnMouseExit)
                {
                    _audioSource.Play();
                    RandomizeSounds();
                    Loop();
                    DestroyAfter();
                    return;
                }

                if (triggerMode == AudioTriggerMode.ThreeD && trigger3DType == AudioTriggerTypes3D.OnMouseExit)
                {
                    _audioSource.Play();
                    Loop();
                    RandomizeSounds();
                    DestroyAfter();
                }
            }

            if (isLooping == true)
            {
                if (StopLoop(AudioTriggerMode.TwoD, AudioTriggerTypes2D.OnMouseExit))
                    return;
                if (StopLoop(AudioTriggerMode.ThreeD, AudioTriggerTypes3D.OnMouseExit))
                    return;
            }
        }

        private void OnMouseUpAsButton()
        {
            if (isLooping == false)
            {
                if (triggerMode == AudioTriggerMode.TwoD && trigger2DType == AudioTriggerTypes2D.OnMouseClick)
                {
                    _audioSource.Play();
                    RandomizeSounds();
                    Loop();
                    DestroyAfter();
                    return;
                }

                if (triggerMode == AudioTriggerMode.ThreeD && trigger3DType == AudioTriggerTypes3D.OnMouseClick)
                {
                    _audioSource.Play();
                    RandomizeSounds();
                    Loop();
                    DestroyAfter();
                }
            }

            if (isLooping == true)
            {
                if (StopLoop(AudioTriggerMode.TwoD, AudioTriggerTypes2D.OnMouseClick))
                    return;
                if (StopLoop(AudioTriggerMode.ThreeD, AudioTriggerTypes3D.OnMouseClick))
                    return;
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (isLooping == false)
            {
                if (triggerMode == AudioTriggerMode.UI && triggerUIType == AudioTriggerTypesUI.OnPointerClick)
                {
                    _audioSource.Play();
                    RandomizeSounds();
                    Loop();
                    DestroyAfter();
                }
            }
            if (isLooping == true)
            {
                StopLoop(AudioTriggerMode.UI, AudioTriggerTypesUI.OnPointerClick);
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (isLooping == false)
            {
                if (triggerMode == AudioTriggerMode.UI && triggerUIType == AudioTriggerTypesUI.OnPointerExit)
                {
                    _audioSource.Play();
                    RandomizeSounds();
                    Loop();
                    DestroyAfter();
                }
            }

            if (isLooping == true)
            {
                StopLoop(AudioTriggerMode.UI, AudioTriggerTypesUI.OnPointerExit);
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (isLooping == false)
            {
                if (triggerMode == AudioTriggerMode.UI && triggerUIType == AudioTriggerTypesUI.OnPointerEnter)
                {
                    _audioSource.Play();
                    RandomizeSounds();
                    Loop();
                    DestroyAfter();
                }
            }

            if (isLooping == true)
            {
                StopLoop(AudioTriggerMode.UI, AudioTriggerTypesUI.OnPointerEnter);
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (isLooping == false)
            {
                if (triggerMode == AudioTriggerMode.ThreeD && trigger3DType == AudioTriggerTypes3D.OnCollisionEnter)
                {
                    _audioSource.Play();
                    RandomizeSounds();
                    Loop();
                    DestroyAfter();
                }
            }

            if (isLooping == true)
            {
                StopLoop(AudioTriggerMode.ThreeD, AudioTriggerTypes3D.OnCollisionEnter);
            }
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (isLooping == false)
            {
                if (triggerMode == AudioTriggerMode.TwoD && trigger2DType == AudioTriggerTypes2D.OnCollisionEnter2D)
                {
                    _audioSource.Play();
                    RandomizeSounds();
                    Loop();
                    DestroyAfter();
                }
            }

            if (isLooping == true)
            {
                StopLoop(AudioTriggerMode.TwoD, AudioTriggerTypes2D.OnCollisionEnter2D);
            }
        }

        private void OnCollisionExit(Collision collision)
        {
            if (isLooping == false)
            {
                if (triggerMode == AudioTriggerMode.ThreeD && trigger3DType == AudioTriggerTypes3D.OnCollisionExit)
                {
                    _audioSource.Play();
                    RandomizeSounds();
                    Loop();
                    DestroyAfter();
                }
            }

            if (isLooping == true)
            {
                StopLoop(AudioTriggerMode.ThreeD, AudioTriggerTypes3D.OnCollisionExit);
            }
        }

        private void OnCollisionExit2D(Collision2D collision)
        {
            if (isLooping == false)
            {
                if (triggerMode == AudioTriggerMode.TwoD && trigger2DType == AudioTriggerTypes2D.OnCollisionExit2D)
                {
                    _audioSource.Play();
                    RandomizeSounds();
                    Loop();
                    DestroyAfter();
                }
            }

            if (isLooping == true)
            {
                StopLoop(AudioTriggerMode.TwoD, AudioTriggerTypes2D.OnCollisionExit2D);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (isLooping == false)
            {
                if (triggerMode == AudioTriggerMode.ThreeD && trigger3DType == AudioTriggerTypes3D.OnTriggerEnter)
                {
                    _audioSource.Play();
                    RandomizeSounds();
                    Loop();
                    DestroyAfter();
                }
            }

            if (isLooping == true)
            {
                StopLoop(AudioTriggerMode.ThreeD, AudioTriggerTypes3D.OnTriggerEnter);
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (isLooping == false)
            {
                if (triggerMode == AudioTriggerMode.TwoD && trigger2DType == AudioTriggerTypes2D.OnTriggerEnter2D)
                {
                    _audioSource.Play();
                    RandomizeSounds();
                    Loop();
                    DestroyAfter();
                }
            }

            if (isLooping == true)
            {
                StopLoop(AudioTriggerMode.TwoD, AudioTriggerTypes2D.OnTriggerEnter2D);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (isLooping == false)
            {
                if (triggerMode == AudioTriggerMode.ThreeD && trigger3DType == AudioTriggerTypes3D.OnTriggerExit)
                {
                    _audioSource.Play();
                    RandomizeSounds();
                    Loop();
                    DestroyAfter();
                }
            }

            if (isLooping == true)
            {
                StopLoop(AudioTriggerMode.ThreeD, AudioTriggerTypes3D.OnTriggerExit);
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (isLooping == false)
            {
                if (triggerMode == AudioTriggerMode.TwoD && trigger2DType == AudioTriggerTypes2D.OnTriggerExit2D)
                {
                    _audioSource.Play();
                    RandomizeSounds();
                    Loop();
                    DestroyAfter();
                }
            }

            if (isLooping == true)
            {
                StopLoop(AudioTriggerMode.TwoD, AudioTriggerTypes2D.OnTriggerExit2D);
            }
        }

        public void Play()
        {
            if (isLooping == false)
            {
                if (triggerMode == AudioTriggerMode.UI && triggerUIType == AudioTriggerTypesUI.OnGameEvent)
                {
                    _audioSource.Play();
                    RandomizeSounds();
                    Loop();
                    DestroyAfter();
                }

                if (triggerMode == AudioTriggerMode.TwoD && trigger2DType == AudioTriggerTypes2D.OnGameEvent)
                {
                    _audioSource.Play();
                    RandomizeSounds();
                    Loop();
                    DestroyAfter();
                }

                if (triggerMode == AudioTriggerMode.ThreeD && trigger3DType == AudioTriggerTypes3D.OnGameEvent)
                {
                    _audioSource.Play();
                    RandomizeSounds();
                    Loop();
                    DestroyAfter();
                }
            }
        }

        private void Stop()
        {
            _audioSource.Stop();
            isLooping = false;
            StopAllCoroutines();
        }

        private void DestroyAfter()
        {
            if (destroyAfterPlay == DestroyType.GameObject)
            {
                if (loop == true)
                    Destroy(gameObject, interval * count);
                else
                    Destroy(gameObject, _audioSource.clip.length);
            }
            if (destroyAfterPlay == DestroyType.Component)
            {
                if (loop == true)
                {
                    Destroy(this, interval * count);
                    Destroy(_audioSource, interval * count);
                }
                else
                {
                    Destroy(this, _audioSource.clip.length);
                    Destroy(_audioSource, _audioSource.clip.length);
                }
            }
        }

        private void RandomizeSounds()
        {
            if (randomizedPitch == true)
            {
                _audioSource.pitch = Random.Range(randomizedPitchRangeMin, randomizedPitchRangeMax);
            }

            if (randomizedVolume == true)
            {
                _audioSource.volume = Random.Range(randomizedVolumeRangeMin, randomizedVolumeRangeMax);
            }
        }

        private void Loop()
        {
            if (loop == true)
            {
                isLooping = true;
                StartCoroutine(LoopSound());
            }
        }

        private bool StopLoop(AudioTriggerMode _mode, AudioTriggerTypesUI _type)
        {
            if (_mode == triggerMode && _type == stopTriggerUIType)
            {
                isLooping = false;
                StopAllCoroutines();
                return true;
            }
            return false;
        }

        private bool StopLoop(AudioTriggerMode _mode, AudioTriggerTypes2D _type)
        {
            if (_mode == triggerMode && _type == stopTrigger2DType)
            {
                isLooping = false;
                StopAllCoroutines();
                return true;
            }
            return false;
        }

        private bool StopLoop(AudioTriggerMode _mode, AudioTriggerTypes3D _type)
        {
            if (_mode == triggerMode && _type == stopTrigger3DType)
            {
                isLooping = false;
                StopAllCoroutines();
                return true;
            }
            return false;
        }

        private IEnumerator LoopSound()
        {
            WaitForSeconds wait = new WaitForSeconds(interval);
            while (isLooping == true)
            {
                for (int i = 0; i < count; i++)
                {
                    RandomizeSounds();
                    _audioSource.Play();
                    yield return wait;
                }
                isLooping = false;
            }
        }
    }

    public enum AudioTriggerTypes3D
    {
        None,
        OnMouseEnter,
        OnMouseExit,
        OnMouseClick,
        OnCollisionEnter,
        OnCollisionExit,
        OnTriggerEnter,
        OnTriggerExit,
        OnGameEvent
    }

    public enum AudioTriggerTypes2D
    {
        None,
        OnMouseEnter,
        OnMouseExit,
        OnMouseClick,
        OnCollisionEnter2D,
        OnCollisionExit2D,
        OnTriggerEnter2D,
        OnTriggerExit2D,
        OnGameEvent
    }

    public enum AudioTriggerTypesUI
    {
        None,
        OnPointerEnter,
        OnPointerExit,
        OnPointerClick,
        OnGameEvent
    }

    public enum AudioTriggerMode
    {
        UI, TwoD, ThreeD, OnStart
    }

    public enum DestroyType
    {
        None, GameObject, Component
    }
}