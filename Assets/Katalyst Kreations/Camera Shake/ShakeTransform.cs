﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KatalystKreations.CameraShake
{
    public class ShakeTransform : MonoBehaviour
    {
        public class ShakeEvent
        {
            private float duration;
            private float timeRemaining;

            private ShakeTransformEventData data;

            public ShakeTransformEventData.Target target
            {
                get { return data.target; }
            }

            private Vector3 noiseOffset;
            public Vector3 noise;

            public ShakeEvent(ShakeTransformEventData _data)
            {
                data = _data;
                duration = data.duration;
                timeRemaining = duration;

                float rand = 32f;

                noiseOffset.x = Random.Range(0f, rand);
                noiseOffset.y = Random.Range(0f, rand);
                noiseOffset.z = Random.Range(0f, rand);
            }

            public void Update()
            {
                float deltaTime = Time.deltaTime;

                timeRemaining -= deltaTime;

                float noiseOffsetDelta = deltaTime * data.frequency;

                noiseOffset.x += noiseOffsetDelta;
                noiseOffset.y += noiseOffsetDelta;
                noiseOffset.z += noiseOffsetDelta;

                noise.x = Mathf.PerlinNoise(noiseOffset.x, 0f);
                noise.y = Mathf.PerlinNoise(noiseOffset.y, 1f);
                noise.z = Mathf.PerlinNoise(noiseOffset.z, 2f);

                noise -= Vector3.one * 0.5f;

                noise *= data.amplitude;

                float agePercent = 1.0f - (timeRemaining / duration);
                noise *= data.blendOverLifetime.Evaluate(agePercent);
            }

            public bool IsAlive()
            {
                return timeRemaining > 0f;
            }
        }

        private List<ShakeEvent> shakeEvents = new List<ShakeEvent>();

        public void AddShakeEvent(ShakeTransformEventData _data)
        {
            shakeEvents.Add(new ShakeEvent(_data));
        }

        public void AddShakeEvent(float _amplitude, float _frequency, float _duration, AnimationCurve _blendOverLifetime, ShakeTransformEventData.Target _target)
        {
            ShakeTransformEventData _data = ScriptableObject.CreateInstance<ShakeTransformEventData>();
            _data.Init(_amplitude, _frequency, _duration, _blendOverLifetime, _target);
            AddShakeEvent(_data);
        }

        private void LateUpdate()
        {
            Vector3 positionOffset = Vector3.zero;
            Vector3 rotationOffset = Vector3.zero;

            for (int i = shakeEvents.Count - 1; i != -1; i--)
            {
                ShakeEvent se = shakeEvents[i];
                se.Update();

                if (se.target == ShakeTransformEventData.Target.Position)
                {
                    positionOffset += se.noise;
                }
                else
                {
                    rotationOffset += se.noise;
                }

                if (se.IsAlive() == false)
                {
                    shakeEvents.RemoveAt(i);
                }
            }

            transform.localPosition = positionOffset;
            transform.localEulerAngles = rotationOffset;
        }
    }
}