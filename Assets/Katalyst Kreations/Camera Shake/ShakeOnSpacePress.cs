﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KatalystKreations.CameraShake
{
    public class ShakeOnSpacePress : MonoBehaviour
    {
        public ShakeTransform st;
        public ShakeTransformEventData data;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                st.AddShakeEvent(data);
            }
        }
    }
}