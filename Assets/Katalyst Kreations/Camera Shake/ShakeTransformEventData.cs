﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KatalystKreations.CameraShake
{
    [CreateAssetMenu(menuName = "Camera Shake/Transform Event Data", fileName = "Shake Transform Event")]
    public class ShakeTransformEventData : ScriptableObject
    {
        public enum Target { Position, Rotation }

        public Target target = Target.Position;

        public float amplitude = 1.0f;
        public float frequency = 1.0f;

        public float duration = 1.0f;

        public AnimationCurve blendOverLifetime = new AnimationCurve(
            new Keyframe(0.0f, 0.0f, Mathf.Deg2Rad * 0.0f, Mathf.Deg2Rad * 720.0f),
            new Keyframe(0.2f, 1.0f),
            new Keyframe(1.0f, 0.0f));

        public void Init(float _amplitude, float _frequency, float _duration, AnimationCurve _blendOverLifetime, Target _target)
        {
            target = _target;

            amplitude = _amplitude;
            frequency = _frequency;

            duration = _duration;

            blendOverLifetime = _blendOverLifetime;
        }
    }
}